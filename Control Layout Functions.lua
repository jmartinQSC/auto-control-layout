--[[
This addition to GetControlLayout will layout your plugin controls in basically the same way as a text controller.
This is helpful when you want to start using your plugin but don't have time to work on the GUI, which can be quite time consuming when a lot of controls are present

The functions GetPosition and GetSize can also be used when you are doing a layout manually, so you don't have to think about pixel dimensions and can instead think in terms of rows and columns.
]]

RowHeight = 16
ColWidth = 36
-- these functions return pixel values for a given row and column size or position, so you don't have to think in pixels when working out the layout
function GetSize(cols,rows )
  return({cols * ColWidth, rows * RowHeight})
end
function GetPosition(cols,rows,hoffset,voffset)
  hoffset = hoffset or 0
  voffset = voffset or 0
  cols = cols - 1 + hoffset
  rows = rows - 1 + voffset
  return({cols * ColWidth, rows * RowHeight})
end

function GetControlLayout(props)
  local ctrls = GetControls()
  local layout   = {}
  local graphics = {}

  -- set what columns to use for Labels and Controls:
  local LabelsCol = 1
  local CtrlsCol = 4


  for i , ctrl in ipairs(ctrls) do
    --add the label for the Control using its Name
    table.insert(graphics, {
        Type = "Text",
        Text = ctrl.Name,
        HTextAlign = "Left",
        Position = GetPosition(LabelsCol , i),
        Size = GetSize(3,1),
      })

    local count = ctrl.Count or 1
    local name = ctrl.Name
    local pretty = name
    for n = 1 , count do
        
        -- for multiple controls with one name - sort the pins into groups and add the number to the name
        if count > 1 then
            pretty = ctrl.Name..'~'..n
            name = ctrl.Name.." "..n      
        end
        
        -- add the control to the control column, multiple controls are added left to right
        layout[name] = {
            Position = GetPosition(CtrlsCol + n - 1 , i),
            PrettyName = pretty
          }
    end
  end

  return layout, graphics
end